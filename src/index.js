const http = require('http');
const getPort = require('get-port');
const ecstatic = require('ecstatic');
const cr = require('callsite-record');
const open = require('open');

const run = async() => {
    try {
        const port = await getPort(3000);
        http
            .createServer(ecstatic({ root: __dirname + '/..' }))
            .listen(port);
        const address = `http://localhost:${port}`;
        console.log(`Serving slides - ${address}`);
        open(address);
    } catch (err) {
        let errStack = await cr({ forError: err }).render();
        console.log(`\nError: ${err.message}\n\n${errStack}`);
    }
}

run();