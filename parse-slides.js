var converter = new showdown.Converter();
var slides = document.body.innerHTML.split("\n\n");
document.body.innerHTML = "";
slides.forEach(slide => document.body.innerHTML += `<div>${converter.makeHtml(slide)}</div>`);