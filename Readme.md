# Example "big" presentation in *.exe

Demo presentation based on [Big](https://github.com/tmcw/big) framework generated to 23-33 MB *.exe file.

![](screenshots/example.png)

---

You may wonder if it is easier just to generate static offline site using `big-presentation-offline` command. The answer is yes. Generating big *.exe file wrapping Node.js engine and trying to run the server on localhost is actually the **overhead**.

It makes more sense when you have some dynamic data that could be fetched, processed and then visualized in the presentation.

Remember that dynamic content is a dependency slides usually don't have. So choose wisely or provide fallback image, because you risk not displaying these visualizations when host PC is offline, or the source expires.

## Build

```shell
> npm i
> npm run pkg
```

Expect *.exe file generated in `.\bin` directory.

## Run

```shell
> npm start
```

This way is useful for running locally during development.

## Next steps

- Write slides using markdown in `index.html`
- Adjust styles in `big.css` if needed
- Generate *.exe
- Take it wherever you want